package bcas.dsa.sort.array;

public class SortedArrayDemo {
	public static void main(String[] args) {
		int[] myArray = { 14, 16, 10, 13, 18, 19 };
		SortedArray SortedArray = new SortedArray();
		System.out.println(SortedArray.minimum(myArray));
		System.out.println(SortedArray.findIndex(myArray));
	}
}